package com.example.examenextempo.extempoalfredozatarain;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.extempoalfredozatarain.R;

import java.util.ArrayList;

public class ListaCalficiaciones extends AppCompatActivity {


    private ListView listCalificaciones;
    private ArrayList<Calficacion> list;
    private AdapterCalificacion adapterCalificacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_calficiaciones);
        initView();
    }


    private void initView() {
        listCalificaciones = (ListView) findViewById(R.id.listCalificaciones);
        list = new DBCalificaciones(this).getCalificaciones();
        adapterCalificacion = new AdapterCalificacion(this, list);
        listCalificaciones.setAdapter(adapterCalificacion);
    }
}
