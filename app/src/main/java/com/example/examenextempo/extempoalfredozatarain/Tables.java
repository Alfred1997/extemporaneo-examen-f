package com.example.examenextempo.extempoalfredozatarain;

public class Tables {

    public static class CalificacionesTabla{
        public static final String TableName = "Calificaciones";
        public static final String ID = "Id";
        public static final String matricula = "Matricula";
        public static final String grupo = "Grupo";
        public static final String calificacion = "Calificaciones";
        public static final String materia = "Materia";
    }
}
